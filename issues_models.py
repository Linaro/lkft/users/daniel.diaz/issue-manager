import datetime
from fakesquad import FakeSquad


class Issues:
    def __init__(self, general_data):
        self.general_data = general_data
        self.issues = list()

    def __len__(self):
        return len(self.issues)

    def get_at(self, index):
        return self.issues[index]

    def get(self, issue_id):
        for issue in self.issues:
            if issue.id == int(issue_id):
                return issue
        return None

    def read_from_database(self):
        query = """
            SELECT *
              FROM issues
             WHERE status
            NOT IN ('RESOLVED')
        """
        self.issues = list()
        for row in self.general_data["db"]["cursor"].execute(query):
            self.issues.append(Issue.row_to_object(row))
        pass

    def rename_issue(self, issue_id, description):
        query = """
            UPDATE issues
               SET description = ?
             WHERE idissue = ?
        """
        self.general_data["db"]["cursor"].execute(query, (description, issue_id))
        self.general_data["db"]["conn"].commit()
        pass

    def create_issue(self, description):
        now = datetime.datetime.now(datetime.timezone.utc)
        delta = datetime.timedelta(days=30)
        later = now + delta
        query = """
            INSERT INTO issues (description, datetime_triage, datetime_review, status)
            VALUES(:description, :dt_triage, :dt_review, :status)
        """
        self.general_data["db"]["cursor"].execute(
            query,
            {
                "description": description,
                "dt_triage": int(now.timestamp()),
                "dt_review": int(later.timestamp()),
                "status": "",
            },
        )
        self.general_data["db"]["conn"].commit()
        pass
        return self.general_data["db"]["cursor"].lastrowid

    def resolve_issue(self, issue_id):
        query = """
            UPDATE issues
               SET status="RESOLVED"
             WHERE idissue=?
        """
        self.general_data["db"]["cursor"].execute(query, (str(issue_id),))
        self.general_data["db"]["conn"].commit()
        pass
        return self.general_data["db"]["cursor"].lastrowid

    def get_failures_for_issue(self, issue_id):
        query = """
            SELECT failures.idtest
              FROM issuesfailures,failures
             WHERE issuesfailures.idtest=failures.idtest
               AND issuesfailures.idissue=?
        """
        failures_id_list = []
        for row in self.general_data["db"]["cursor"].execute(query, (str(issue_id),)):
            failures_id_list.append(row[0])
        pass
        return failures_id_list

    def create_note(self, issue_id, note):
        now = datetime.datetime.now(datetime.timezone.utc)
        query = """
            INSERT INTO notes (datetime_entry, note)
                 VALUES (?, ?)
        """
        self.general_data["db"]["cursor"].execute(query, (int(now.timestamp()), note))
        self.general_data["db"]["conn"].commit()
        note_id = self.general_data["db"]["cursor"].lastrowid
        pass

        query = """
            INSERT INTO issuesnotes
                 VALUES (?, ?)
        """
        self.general_data["db"]["cursor"].execute(query, (issue_id, note_id))
        self.general_data["db"]["conn"].commit()
        pass

        return

    def create_link(self, issue_id, linktype, link):
        query = """
            INSERT INTO issueslinks
                 VALUES (?, ?, ?)
        """
        self.general_data["db"]["cursor"].execute(query, (issue_id, linktype, link))
        self.general_data["db"]["conn"].commit()
        pass
        return

    def extra_info(self, issue_id):
        # Number of failures
        query = """
            SELECT COUNT(idtest)
              FROM issuesfailures
             WHERE idissue=?
        """
        self.general_data["db"]["cursor"].execute(query, (str(issue_id),))
        row = self.general_data["db"]["cursor"].fetchone()
        total_failures = 0
        if row is not None:
            total_failures = row[0]
        pass

        # Build versions
        build_ids = []
        build_versions = []
        query = """
            SELECT DISTINCT(squad_build_id)
              FROM failures,issuesfailures
             WHERE failures.idtest=issuesfailures.idtest
               AND issuesfailures.idissue=:issue_id
        """
        for row in self.general_data["db"]["cursor"].execute(query, (str(issue_id),)):
            build_ids.append(row[0])
        for squad_build_id in build_ids:
            build_versions.append(
                self.general_data["squad"]["fake"].resolve_build(
                    self.general_data, squad_build_id
                )
            )

        # First and last seen
        query = """
            SELECT MIN(datetime_created),MAX(datetime_created)
              FROM builds
             WHERE builds.squad_build_id IN (
                   SELECT DISTINCT(squad_build_id)
                     FROM failures,issuesfailures
                    WHERE failures.idtest=issuesfailures.idtest
                      AND issuesfailures.idissue=?
                   )
        """
        self.general_data["db"]["cursor"].execute(query, (str(issue_id),))
        row = self.general_data["db"]["cursor"].fetchone()
        first_datetime = None
        last_datetime = None
        if row is not None:
            first_datetime, last_datetime = row
        pass

        # List of environments
        query = """
            SELECT DISTINCT(failures.squad_environment_id)
              FROM failures,issuesfailures
             WHERE failures.idtest=issuesfailures.idtest
               AND issuesfailures.idissue=?;
        """
        environment_ids = []
        environments = []
        for row in self.general_data["db"]["cursor"].execute(query, (str(issue_id),)):
            environment_ids.append(row[0])
        for squad_environment_id in environment_ids:
            environments.append(
                self.general_data["squad"]["fake"].resolve_environment(
                    self.general_data, squad_environment_id
                )
            )

        # Notes
        query = """
            SELECT *
              FROM notes,issuesnotes
             WHERE notes.idnote=issuesnotes.idnote
               AND idissue=?;
        """
        notes = {}
        for row in self.general_data["db"]["cursor"].execute(query, (str(issue_id),)):
            notes[row[1]] = row[2]

        # Links
        query = """
            SELECT description,link
              FROM issueslinks,linktypes
             WHERE issueslinks.idlinktype=linktypes.idlinktype
               AND idissue=?;
        """
        links = []
        for row in self.general_data["db"]["cursor"].execute(query, (str(issue_id),)):
            links.append((row[0], row[1]))

        return {
            "failures_count": total_failures,
            "build_versions": build_versions,
            "environments": environments,
            "datetime_first": first_datetime,
            "datetime_last": last_datetime,
            "notes": notes,
            "links": links,
        }


class Issue:
    # id
    # description
    # dttriage
    # dtreview
    # status

    def row_to_object(row):
        i = Issue()
        pass
        (i.id, i.description, i.dttriage, i.dtreview, i.status) = row
        return i

    def __init__(self, id=None):
        super().__init__()
        if id:
            self.id = id

    def set_description(self, description):
        self.description = description
