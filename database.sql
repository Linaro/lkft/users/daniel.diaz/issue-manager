CREATE TABLE failures (
  idtest INTEGER PRIMARY KEY,
  testsuite VARCHAR(255) NOT NULL,
  short_name VARCHAR(255) NOT NULL,
  log TEXT,
  confidence_score FLOAT,
  confidence_count INT,
  squad_build_id INT,
  squad_environment_id INT,
  squad_test_run_id INT,
  squad_suite_id INT,
  squad_metadata_id INT
);

CREATE TABLE issues (
  idissue INTEGER PRIMARY KEY AUTOINCREMENT,
  description VARCHAR(255),
  datetime_triage VARCHAR(30),
  datetime_review VARCHAR(30),
  status VARCHAR(20)
);

CREATE TABLE issuesfailures (
  idissue INTEGER,
  idtest INTEGER,
  PRIMARY KEY (idissue, idtest)
);

CREATE TABLE issueslinks (
  idissue INTEGER,
  idlinktype INTEGER,
  link VARCHAR(255),
  PRIMARY KEY (idissue, idlinktype)
);

CREATE TABLE issuesnotes (
  idissue INTEGER,
  idnote INTEGER,
  PRIMARY KEY (idissue, idnote)
);

CREATE TABLE issuestags (
  idissue INTEGER,
  idtag INTEGER,
  PRIMARY KEY (idissue, idtag)
);

CREATE TABLE linktypes (
  idlinktype INTEGER PRIMARY KEY,
  description VARCHAR(255)
);

CREATE TABLE notes (
  idnote INTEGER PRIMARY KEY,
  datetime_entry VARCHAR(30),
  note TEXT
);

CREATE TABLE tags (
  idtag INTEGER PRIMARY KEY,
  tag TEXT
);

CREATE TABLE builds (
  squad_build_id INTEGER PRIMARY KEY,
  squad_project_id INTEGER,
  datetime_created VARCHAR(30),
  status VARCHAR(20)
);

--now="$(date -d "now" +%s)"
--later="$(date -d "now + 30 days" +%s)"
--INSERT INTO issues VALUES(1, "XHCI static declaration follows non-static declaration", ${now}, ${later}, "");
--INSERT INTO issuesfailures VALUES(1, 3700373719);

--INSERT INTO builds VALUES(155008, 1764, 1684784973, "FETCHED");
--INSERT INTO builds VALUES(155745, 1764, 1685302239, "FETCHED");
INSERT INTO linktypes VALUES(1, "LKFT report");
INSERT INTO linktypes VALUES(2, "Report");
INSERT INTO linktypes VALUES(3, "Test log/artifacts");
INSERT INTO linktypes VALUES(4, "Bug tracker");
INSERT INTO linktypes VALUES(5, "General");
