#!/bin/bash

# Environment defaults
# If not defined before this point, they will be set to this:
if [ ! -v VISOR_LORE_DIR ]; then
  export VISOR_LORE_DIR="/data/lore-stable"
fi
if [ ! -v VISOR_DATA_DIR ]; then
  export VISOR_DATA_DIR="${HOME}/.local/lkft-visor"
fi
if [ ! -v VISOR_CACHE_EXPIRATION ]; then
  export VISOR_CACHE_EXPIRATION=30
fi
if [ ! -v VISOR_CACHE_DOWNLOADS ]; then
  export VISOR_CACHE_DOWNLOADS="${VISOR_DATA_DIR}/.downloads"
fi
if [ ! -v SQUAD_SERVER ]; then
  export SQUAD_SERVER="https://qa-reports.linaro.org"
fi
if [ ! -v SQUAD_GROUP ]; then
  export SQUAD_GROUP="lkft"
fi

# $1: git repo url
# $2: remote name
# $3: directory
# $4: branch to sync (optional)
#
# From the environment:
# $VISOR_CACHE_EXPIRATION
# It will optionally take this value if
# set.
clone_or_update() {
  git_repo="$1"
  remote="$2"
  directory="$3"
  branch="${4:-""}"

  hash="$(hash_string "$1$2$3")"
  cache_stale="$(is_cache_stale "${VISOR_DATA_DIR}/${hash}.updated")"
  if [ ! -d "${directory}" ]; then
    cache_stale="true"
  fi
  if [ "${cache_stale}" == "false" ]; then
    return
  fi

  if [ ! -d "${directory}" ]; then
    git clone -o "${remote}" "${git_repo}" "${directory}" > /dev/null
  else
    pushd "${directory}" > /dev/null || exit 1
    if ! git remote -v | grep -q "^${remote}\s"; then
      git remote add "${remote}" "${git_repo}" > /dev/null
    fi
    git fetch "${remote}" > /dev/null 2>&1
    popd > /dev/null || exit 1
  fi

  if [ -n "${branch}" ]; then
    if [ -d "${directory}" ]; then
      pushd "${directory}" > /dev/null || exit 1
      git checkout -B "${branch}" "${remote}/${branch}" > /dev/null 2>&1
      popd > /dev/null || exit 1
    fi
  fi

  touch "${VISOR_DATA_DIR}/${hash}.updated"
}

# $1: file to modify
# $2: key
# $3: value
replace_line() {
  if ! grep -q "^[[:space:]]*$2" "$1" 2>/dev/null; then
    echo "$2$3" >> "$1"
    return
  fi

  line="$(grep "^[[:space:]]*$2" "$1")"
  whitespace="$(echo "${line}" | grep -Eo "^[[:space:]]*")"

  sed -i -e "s#^${whitespace}${2}.*#${whitespace}${2}${3}#" "$1"
}

# $1: url
# $2: machine
get_bundle_from_tuxpub() {
  curl -sSL https://gitlab.com/Linaro/lkft/rootfs/dir2bundle/-/archive/master/dir2bundle-master.tar.gz | tar zxf - --overwrite

  machine="$2"
  dir_json="$(mktemp)"
  bundle_json="$(mktemp)"

  wget -qO "${dir_json}" "$1/?export=json"

  jq -r '.files[].Url' "${dir_json}" | sed -e "s|$1/||g" | ./dir2bundle-master/dir2bundle "${machine}" > "${bundle_json}"

  cat "${bundle_json}"
}

# $1: parameter to extract
#     This argument is required
# $2: location of bundle.json
#     This is optional. Defaults to
#     bundle.json in the current
#     directory.
get_from_bundle() {
  bundle_json="bundle.json"
  if [ $# -gt 1 ]; then
    bundle_json="$2"
  fi
  jq -r ".$1" "${bundle_json}"
}

# https://unix.stackexchange.com/a/451250
# $1: reference date
hdate() {
  awk -v date="$(date +%s -d "$1")" -v now="$(date +%s)" '
    BEGIN {  diff = now - date;
       if (diff > (48*60*60)) printf "%.0f days ago", diff/(24*60*60);
       else if (diff > (24*60*60)) printf "%.0f hours ago", diff/(60*60);
       else if (diff > (60*60)) printf "%.0f hours ago", diff/(60*60);
       else if (diff > 60) printf "%.0f minutes ago", diff/60;
       else printf "%s seconds ago", diff;
    }'
}

# Get the hash of any string
hash_string() {
  echo -n "$1" | sha1sum | cut -c1-40
}

# $1: URL to download and cache
# $2: cache validity, in minutes
#     This is optional. Defaults to 30
#     minutes.
#
# From the environment:
# $VISOR_CACHE_EXPIRATION
# It will optionally take this value if
# set. The positional argument takes
# precedence over this.
# $FORCE_UNCACHED
# When set to true it will ignore all
# caching of artifacts and will fetch
# things anew.
#
# Returns the absolute path and file name of the downloaded resource.
download_and_cache() {
  url="${1}"
  hashed_url="$(hash_string "${url}")"
  validity="${VISOR_CACHE_EXPIRATION:-30}"
  validity="${2:-30}"

  output_file="${VISOR_CACHE_DOWNLOADS}/${hashed_url}"

  if [ -v FORCE_UNCACHED ] && [ "${FORCE_UNCACHED}" = "true" ]; then
    cache_stale="true"
  else
    cache_stale="$(is_cache_stale "${output_file}")"
  fi

  if [ ! -s "${output_file}" ] || [ "${cache_stale}" = "true" ]; then
    curl -sSLf -o "${output_file}" "${url}" || echo "Error fetching ${url}"
  fi
  if [ -s "${output_file}" ]; then
    readlink -e "${output_file}"
  fi
}

# $1: URL to download and cache
# $2: Number of pages to download
#     This is optional. If set to 0, it
#     will download all available
#     pages.
# $3: Cache validity, in minutes
#     This is optional. Defaults to 30
#     minutes.
# $4: Verbosity level. Defaults to 0
#     (quiet). Level 1 prints page being
#     retrieved.
#
# From the environment:
# $VISOR_CACHE_EXPIRATION
# It will optionally take this value if
# set. The positional argument takes
# precedence over this.
# $FORCE_UNCACHED
# When set to true it will ignore all
# caching of artifacts and will fetch
# things anew.
#
# Returns the absolute path and file name of the downloaded resource.
squad_download_and_cache() {
  url_canonical="${1}"
  hashed_url="$(hash_string "${url_canonical}")"
  number_of_pages="${2:-200}"
  validity="${VISOR_CACHE_EXPIRATION:-3}"
  validity="${3:-30}"
  verbosity_level="${4:-0}"

  output_file="${VISOR_CACHE_DOWNLOADS}/${hashed_url}"

  if [ -v FORCE_UNCACHED ] && [ "${FORCE_UNCACHED}" = "true" ]; then
    cache_stale="true"
  else
    cache_stale="$(is_cache_stale "${output_file}")"
  fi

  PAGES=()
  page_number=1
  url="${1}"
  if [ "${url/offset=/}" = "${url}" ]; then
    if [ "${url/\?/}" = "${url}" ]; then
      url="${url}?offset=0"
    else
      url="${url}&offset=0"
    fi
  fi

  if [ "${verbosity_level}" -ge 1 ]; then
    echo "Fetching ${url}..." >&2
  fi

  CURL_OPTIONS=()
  if [ "${verbosity_level}" -ge 5 ]; then
    CURL_OPTIONS=(--verbose)
  fi

  if [ ! -s "${output_file}" ] || [ "${cache_stale}" = "true" ]; then
    while true; do
      hashed_url_page="$(hash_string "${url}")"
      output_url_page="${VISOR_CACHE_DOWNLOADS}/${hashed_url_page}"

      if [ "${verbosity_level}" -ge 1 ]; then
        echo "Page ${page_number}/${number_of_pages}..." >&2
      fi

      # shellcheck disable=SC2048 disable=SC2086
      if curl -sSLf ${CURL_OPTIONS[*]} --retry-delay 10 --retry 30 -o "${output_url_page}" "${url}"; then
        PAGES+=("${output_url_page}")
        if [ "${verbosity_level}" -ge 2 ]; then
          echo "Page saved in ${output_url_page}" >&2
        fi
      else
        echo "Error fetching ${url}" >&2
        # This should be return, but we want to keep
        # partial results from failures_with_confidence:
        break
      fi

      next_url="$(jq -r .next "${output_url_page}")"
      if [ "${next_url}" == "null" ]; then
        break
      fi

      if [ "${number_of_pages}" -ge 1 ] && [ "${page_number}" -ge "${number_of_pages}" ]; then
        break
      fi

      page_number=$((page_number + 1))
      url="${next_url}"
    done

    if [ "${page_number}" -ge 1 ]; then
      cat "${PAGES[@]}" > "${output_file}"
    fi
  fi

  if [ -s "${output_file}" ]; then
    readlink -e "${output_file}"
  fi
}

# $1: file to check
# $2: cache expiration
is_cache_stale() {
  output_file="$1"
  validity="${2:-30}"

  cache_stale="true"
  if [ -f "${output_file}" ]; then
    dt_file="$(stat -c %Y "${output_file}")"
    dt_now="$(date +%s)"
    dt_diff=$(((dt_now - dt_file) / 60))
    [ "${dt_diff}" -le "${validity}" ] && cache_stale="false"
  fi
  echo "${cache_stale}"
}

# $1: stable-rc branch (or "mainline"
#     or "next"), like "5.15"
get_squad_project_url_by_branch() {
  [ -z "$1" ] && return

  branch_slug="$(get_squad_slug_by_branch "$1")"

  # Get SQUAD group
  url="${SQUAD_SERVER}/api/groups/?slug=${SQUAD_GROUP}"
  group_json="$(download_and_cache "${url}")"
  group_id="$(jq -r '.results[0].id' "${group_json}")"

  if [ -z "${group_id}" ]; then
    return
  fi

  # Get SQUAD project's URL
  url="${SQUAD_SERVER}/api/projects/?group=${group_id}&slug=${branch_slug}"
  project_json="$(download_and_cache "${url}")"
  project_url="$(jq -r '.results[0].url' "${project_json}")"

  echo "${project_url}"
}

# $1: stable-rc branch (or "mainline"
#     or "next"), like "5.15"
get_squad_slug_by_branch() {
  [ -z "$1" ] && return

  case $1 in
    4.9)  url="linux-stable-rc-linux-4.9.y" ;;
    4.14) url="linux-stable-rc-linux-4.14.y" ;;
    4.19) url="linux-stable-rc-linux-4.19.y" ;;
    5.4)  url="linux-stable-rc-linux-5.4.y" ;;
    5.10) url="linux-stable-rc-linux-5.10.y" ;;
    5.15) url="linux-stable-rc-linux-5.15.y" ;;
    5.19) url="linux-stable-rc-linux-5.19.y" ;;
    6.0)  url="linux-stable-rc-linux-6.0.y" ;;
    6.1)  url="linux-stable-rc-linux-6.1.y" ;;
    6.2)  url="linux-stable-rc-linux-6.2.y" ;;
    6.3)  url="linux-stable-rc-linux-6.3.y" ;;
    6.4)  url="linux-stable-rc-linux-6.4.y" ;;
    mainline) url="linux-mainline-master" ;;
    linux-next) url="linux-next-master" ;;
  esac

  echo "${url}"
}

# $1: stable-rc branch (or "mainline"
#     or "next"), like "5.15"
get_squad_project_slug_by_branch() {
  [ -z "$1" ] && return
  url="$(get_squad_project_url_by_branch "$1")"
  project_json="$(download_and_cache "${url}/")"
  slug="$(jq -r .slug "${project_json}")"

  echo "${slug}"
}

get_squad_api() {
  echo "${SQUAD_SERVER}/api"
}

# $1: the makefile_version
#     Expressed like "6.0.1-rc1"
get_branch_from_makefile_version() {
  echo "$1" | cut -d. -f1-2
}

# $1: Visor command to align to the right
# $2..$n: Line to print
echo_cmd() {
  cmd="$1"
  shift
  content="$*"
  columns="$(tput cols)"
  cmd_str="[${cmd}]"
  cmd_str_len="${#cmd_str}"
  available_space=$((columns - cmd_str_len - 1 - 5))
  printf "%-${available_space}s %s\n" "${content}" "${cmd_str}"
}

# $1: branch
get_builds_json_from_branch() {
  branch="$1"
  url="$(get_squad_project_url_by_branch "${branch}")"
  download_and_cache "${url}/builds"
}

# $1: kernelversion
get_builds_json_from_kernelversion() {
  kernelversion="$1"

  branch="$(get_branch_from_makefile_version "${kernelversion}")"
  get_builds_json_from_branch "${branch}"
}

# $1: kernelversion
get_build_id_for_kernelversion() {
  if [ -z "$1" ]; then return; fi
  kernelversion="$1"

  builds_json="$(get_builds_json_from_kernelversion "${kernelversion}")"

  build_file="${VISOR_DATA_DIR}/user/builds/${kernelversion}"
  if [ -s "${build_file}" ]; then
    build_id="$(cat "${build_file}")"
    metadata_url="$(jq -r ".results[] | select(.version == \"${build_id}\") | .metadata" "${builds_json}")"
  else
    # Find build with given kernel_version.
    # Iterate and stop when found.
    # (build_id and metadata_url are byproducts of this iteration.)
    while read -r metadata_url; do
      metadata_json="$(download_and_cache "${metadata_url}")"
      build_id="$(jq -r "select(.kernel_version == \"${kernelversion}\") | .git_describe" "${metadata_json}")"
      [ -n "${build_id}" ] && break
    done < <(jq -r ".results[].metadata" "${builds_json}")
  fi
  echo "${build_id}"
}

# $1: kernelversion
get_build_url_for_kernelversion() {
  if [ -z "$1" ]; then return; fi
  kernelversion="$1"

  builds_json="$(get_builds_json_from_kernelversion "${kernelversion}")"

  build_file="${VISOR_DATA_DIR}/user/builds/${kernelversion}"
  if [ -s "${build_file}" ]; then
    build_id="$(cat "${build_file}")"
    build_url="$(jq -r ".results[] | select(.version == \"${build_id}\") | .url" "${builds_json}")"
  else
    # Find build with given kernel_version.
    # Iterate and stop when found.
    # (build_id and metadata_url are byproducts of this iteration.)
    while read -r metadata_url; do
      metadata_json="$(download_and_cache "${metadata_url}")"
      build_id="$(jq -r "select(.kernel_version == \"${kernelversion}\") | .git_describe" "${metadata_json}")"
      [ -n "${build_id}" ] && break
    done < <(jq -r ".results[].metadata" "${builds_json}")
    build_url="$(jq -r ".results[] | select(.metadata == \"${metadata_url}\") | .url" "${builds_json}")"
  fi
  echo "${build_url}"
}

# $1: moniker
# $2: build
get_build_url_for_build() {
  moniker="$1"
  kernelversion="$2"

  builds_json="$(get_builds_json_from_branch "${moniker}")"

  # Find build with given kernel_version.
  # Iterate and stop when found.
  # (build_id and metadata_url are byproducts of this iteration.)
  while read -r metadata_url; do
    metadata_json="$(download_and_cache "${metadata_url}")"
    build_id="$(jq -r "select(.git_describe == \"${kernelversion}\") | .git_describe" "${metadata_json}")"
    [ -n "${build_id}" ] && break
  done < <(jq -r ".results[].metadata" "${builds_json}")
  build_url="$(jq -r ".results[] | select(.metadata == \"${metadata_url}\") | .url" "${builds_json}")"
  echo "${build_url}"
}

# $1: kernelversion
get_baseline_id_for_kernelversion() {
  kernelversion="$1"
  build_url="$(get_build_url_for_kernelversion "${kernelversion}")"

  baseline_file="${VISOR_DATA_DIR}/user/baselines/${kernelversion}"
  if [ -s "${baseline_file}" ]; then
    baseline_id="$(cat "${baseline_file}")"
  else
    build_status_json="$(download_and_cache "${build_url}/status/")"
    baseline_url="$(jq -r .baseline "${build_status_json}")"
    baseline_json="$(download_and_cache "${baseline_url}")"
    baseline_id="$(jq -r .version "${baseline_json}")"
  fi

  echo "${baseline_id}"
}

# $1: moniker
# $2: kernelversion
get_baseline_id_for_build() {
  moniker="$1"
  kernelversion="$2"
  build_url="$(get_build_url_for_build "${moniker}" "${kernelversion}")"

  baseline_file="${VISOR_DATA_DIR}/user/baselines/${kernelversion}"
  if [ -s "${baseline_file}" ]; then
    baseline_id="$(cat "${baseline_file}")"
  else
    build_status_json="$(download_and_cache "${build_url}/status/")"
    baseline_url="$(jq -r .baseline "${build_status_json}")"
    baseline_json="$(download_and_cache "${baseline_url}")"
    baseline_id="$(jq -r .version "${baseline_json}")"
  fi

  echo "${baseline_id}"
}

# $1: branch
# $2: git_describe
get_squad_build_id_for_git_describe() {
  branch="$1"
  git_describe="$2"

  builds_json="$(get_builds_json_from_branch "${branch}")"

  squad_build_id="$(jq -r ".results[] | select(.version == \"${git_describe}\") | .id" "${builds_json}")"

  echo "${squad_build_id}"
}

# $1: branch
# $2: build
# $3: baseline
get_metrics_regressions() {
  branch=$1
  build=$2
  baseline=$3

  base_url="$(get_squad_project_url_by_branch "${branch}")"
  baseline_id="$(get_squad_build_id_for_git_describe "${branch}" "${baseline}")"
  build_id="$(get_squad_build_id_for_git_describe "${branch}" "${build}")"

  url="${base_url}/compare_builds/?force=true&by=metrics&baseline=${baseline_id}&to_compare=${build_id}"
  metrics_regressions_list="$(download_and_cache "${url}")"

  keys="$(jq -r '.regressions | keys[]' "${metrics_regressions_list}" | sort -u | tr '\n' ' ')"

  # shellcheck disable=SC2048
  for arch in ${keys[*]}; do
    jq -r ".regressions.${arch}.build[]" "${metrics_regressions_list}" | while read -r kconfig; do
      echo "${arch}/${kconfig}"
    done
  done
}

squad_environment_to_string() {
  environment_url="$1"
  environment_json="$(download_and_cache "${environment_url}")"
  jq -r .slug "${environment_json}"
}
