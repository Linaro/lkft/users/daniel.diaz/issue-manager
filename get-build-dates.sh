#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
. "${ROOT_DIR}/lib.sh"

set -e
BUILD_ID="$1"

# Get build date and project
build_json="$(download_and_cache "${SQUAD_SERVER}/api/builds/${BUILD_ID}/")"

created_at="$(jq -r .created_at "${build_json}")"
ts_created_at="$(date +%s -d "${created_at}")"

squad_project_url="$(jq -r .project "${build_json}")"
squad_project="${squad_project_url/${SQUAD_SERVER}/}"
squad_project="${squad_project/\/api\/projects\//}"
squad_project="${squad_project/\//}"

echo "INSERT INTO builds VALUES(${BUILD_ID}, ${squad_project}, ${ts_created_at}, \"FETCHED\");"
