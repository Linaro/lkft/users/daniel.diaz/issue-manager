#!/usr/bin/env python3
import json
import sqlite3
import sys

if len(sys.argv) < 2:
    print("Need a file.json as argument")
    sys.exit(0)

filename = sys.argv[1]
# file = open("failures-confidence-v6.3.4-rc1.results.json")
file = open(filename)
# contents = file.read()
# json_data = json.loads(contents)
json_data = json.load(file)


def get_squad_api_id(url):
    elements = url.split("/")
    return elements[-2]


con = sqlite3.connect("lim.db")
cur = con.cursor()

for page in json_data:
    for failure in page["results"]:
        testsuite = failure["name"].split("/")[0]
        try:
            query_form = "INSERT INTO failures VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);"
            res = cur.execute(
                query_form,
                [
                    int(failure["id"]),
                    testsuite,
                    failure["short_name"],
                    failure["log"],
                    failure["confidence"]["score"],
                    int(failure["confidence"]["count"]),
                    int(get_squad_api_id(failure["build"])),
                    int(get_squad_api_id(failure["environment"])),
                    int(get_squad_api_id(failure["test_run"])),
                    int(get_squad_api_id(failure["suite"])),
                    int(get_squad_api_id(failure["metadata"])),
                ],
            )
            print(".", end="")
        except sqlite3.IntegrityError as e:
            print("!", end="")
            pass
con.commit()
