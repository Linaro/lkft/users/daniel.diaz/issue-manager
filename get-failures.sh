#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
. "${ROOT_DIR}/lib.sh"

set -e
BUILD_URL="$1"

url_noprot="${BUILD_URL#*://*}"
export SQUAD_SERVER="https://${url_noprot%%/*}"

BUILD_ID="$(~/devel/lkft-tools/bin/build_info.py "${BUILD_URL}" | grep ^ID: | awk '{print $2}')"

"${ROOT_DIR}/fetch-failures.sh" "${BUILD_ID}"
