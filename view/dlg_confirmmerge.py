from PySide6 import QtWidgets

from failures_models import *
from issues_models import *


class ConfirmMergeDialog(QtWidgets.QDialog):
    def __init__(self, general_data):
        self.general_data = general_data
        self.db_cursor = self.general_data["db"]

    def get_dialog(self, failure_id_list, issue_id):
        self.dialog = self.general_data["loader"].load("dlg-confirmmerge.ui", None)

        if len(failure_id_list) == 1:
            failure = self.general_data["active_failures"].get(failure_id_list[0])
            testname = f"{failure.testsuite}/{failure.short_name}"
        else:
            testname = str(len(failure_id_list)) + " failures"

        issue = self.general_data["all_issues"].get(issue_id)

        self.dialog.setWindowTitle("Confirm merge %s" % testname)
        msg = self.dialog.lbl_msg
        msg.setText(f"Do you want to merge\n{testname}\nwith\n{issue.description}\n?")
        return self.dialog
