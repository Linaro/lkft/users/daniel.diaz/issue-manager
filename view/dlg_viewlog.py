from PySide6 import QtWidgets

from failures_models import *
from fakesquad import FakeSquad
from issues_models import *


class ViewLogDialog(QtWidgets.QDialog):
    def __init__(self, general_data):
        self.general_data = general_data
        self.db_cursor = self.general_data["db"]["cursor"]

    def get_dialog(self, failure_id):
        self.dialog = self.general_data["loader"].load("dlg-viewlog.ui", None)
        self.dialog.setWindowTitle(f"Log for %d" % int(failure_id))
        log = FakeSquad.fetch_full_log(self.general_data, failure_id)
        text_log = self.dialog.txt_log
        text_log.setPlainText(log)
        return self.dialog
