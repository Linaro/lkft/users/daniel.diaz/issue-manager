class Failures:
    def __init__(self, general_data):
        self.general_data = general_data
        self.failures = list()

    def __len__(self):
        return len(self.failures)

    def get_at(self, index):
        return self.failures[index]

    def get(self, failure_id):
        for failure in self.failures:
            if failure.id == int(failure_id):
                return failure
        return None

    def reset(self):
        self.failures = list()

    def append(self, failure):
        self.failures.append(failure)

    def fetch_all(self):
        query = "SELECT * FROM failures;"
        self.reset()
        for row in self.general_data["db"]["cursor"].execute(query):
            self.append(Failure.row_to_object(row))

    def fetch_active(self):
        query = "SELECT * FROM failures WHERE failures.idtest NOT IN (SELECT idtest FROM issuesfailures);"
        self.reset()
        for row in self.general_data["db"]["cursor"].execute(query):
            self.append(Failure.row_to_object(row))

    def merge_with_issue(self, failure_id_list, issue_id):
        query = "INSERT INTO issuesfailures VALUES(?, ?);"
        self.general_data["db"]["cursor"].executemany(
            query, map(lambda failure: (issue_id, failure), failure_id_list)
        )
        self.general_data["db"]["conn"].commit()

    def unmerge_from_issue(self, failure_id_list, issue_id):
        query = "DELETE FROM issuesfailures WHERE idissue=? AND idtest=?;"
        self.general_data["db"]["cursor"].executemany(
            query, map(lambda failure: (issue_id, failure), failure_id_list)
        )
        self.general_data["db"]["conn"].commit()


class Failure:
    # id
    # testsuite
    # short_name
    # log
    # confidence_score
    # confidence_count
    # api_build
    # api_environment
    # api_test_run
    # api_suite
    # api_metadata

    def row_to_object(row):
        f = Failure()
        pass
        (
            f.id,
            f.testsuite,
            f.short_name,
            f.log,
            f.confidence_score,
            f.confidence_count,
            f.squad_build_id,
            f.squad_environment_id,
            f.squad_test_run_id,
            f.squad_suite_id,
            f.squad_metadata_id,
        ) = row
        return f

    def __init__(self, id=None):
        super().__init__()
        if id:
            self.id = id

    def set_testsuite(self, testsuite):
        self.testsuite = testsuite

    def set_short_name(self, short_name):
        self.short_name = short_name

    def set_log(self, log):
        self.log = log

    def set_confidence_score(self, confidence_score):
        self.confidence_score = confidence_score

    def set_confidence_count(self, confidence_count):
        self.confidence_count = confidence_count

    def set_squad_build_id(self, squad_build_id):
        self.squad_build_id = squad_build_id

    def set_squad_environment(self, squad_environment):
        self.squad_environment_id = squad_environment

    def set_squad_test_run(self, squad_test_run_id):
        self.squad_test_run_id = squad_test_run_id

    def set_squad_suite(self, squad_suite_id):
        self.squad_suite_id = squad_suite_id

    def set_squad_metadata(self, squad_metadata_id):
        self.squad_metadata_id = squad_metadata_id
