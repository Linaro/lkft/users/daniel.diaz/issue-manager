import datetime
import hashlib
import json
import os

import requests


class FakeSquad:
    def __init__(self, general_data):
        self.build_versions = {}
        self.environments = {}
        self.session = requests.Session()
        self.general_data = general_data
        pass

    def download_and_cache(general_data, session, url):
        config_get = general_data["fx"]["config_get"]
        cache_dir = config_get(general_data["config"], "squad/cache_dir")
        cache_valid = config_get(general_data["config"], "squad/cache_valid")

        hash = hashlib.sha256(url.encode("utf-8")).hexdigest()
        cached_file = os.path.join(cache_dir, hash)
        try:
            statinfo = os.stat(cached_file)
            if (statinfo.st_mtime + cache_valid) < datetime.datetime.now().timestamp():
                statinfo = None
        except FileNotFoundError as e:
            statinfo = None

        if statinfo:
            print(".", end="")
            fp = open(cached_file, "r")
            data = fp.read()
        else:
            print("!", end="")
            response = session.get(url)
            data = response.content.decode("utf-8")
            fp = open(cached_file, "w")
            fp.write(data)
        fp.close()
        return data

    def fetch_full_log(general_data, test_id):
        if not test_id:
            return None

        s = requests.Session()

        config_get = general_data["fx"]["config_get"]
        squad_server = config_get(general_data["config"], "squad/server")

        test_url = f"{squad_server}/api/tests/{test_id}/"

        # test_response = s.get(test_url)
        # test_data = test_response.content.decode("utf-8")
        test_data = FakeSquad.download_and_cache(general_data, s, test_url)
        test_json = json.loads(test_data)

        testrun_url = test_json["test_run"]
        # testrun_response = s.get(testrun_url)
        # testrun_data = testrun_response.content.decode("utf-8")
        testrun_data = FakeSquad.download_and_cache(general_data, s, testrun_url)
        testrun_json = json.loads(testrun_data)

        testlog_url = testrun_json["log_file"]
        # testlog_response = s.get(testlog_url)
        # testlog_data = testlog_response.content.decode("utf-8")
        testlog_data = FakeSquad.download_and_cache(general_data, s, testlog_url)
        return testlog_data

    def resolve_build(self, general_data, squad_build_id):
        if not squad_build_id:
            return None

        config_get = general_data["fx"]["config_get"]
        squad_server = config_get(general_data["config"], "squad/server")

        if squad_build_id not in self.build_versions:
            build_url = f"{squad_server}/api/builds/{squad_build_id}/"
            build_data = FakeSquad.download_and_cache(
                general_data, self.session, build_url
            )
            build_json = json.loads(build_data)
            self.build_versions[squad_build_id] = build_json["version"]
            return build_json["version"]
        else:
            return self.build_versions[squad_build_id]

    def resolve_project(general_data, squad_project_id):
        if not squad_project_id:
            return None

        config_get = general_data["fx"]["config_get"]
        squad_server = config_get("squad/server")

        s = requests.Session()

        project_url = f"{squad_server}/api/projects/{squad_project_id}/"
        project_data = FakeSquad.download_and_cache(general_data, s, project_url)
        project_json = json.loads(project_data)
        return project_json["slug"]

    def resolve_environment(self, general_data, environment_id):
        if not environment_id:
            return None

        config_get = general_data["fx"]["config_get"]
        squad_server = config_get(general_data["config"], "squad/server")

        # s = requests.Session()

        if environment_id not in self.environments:
            environment_url = f"{squad_server}/api/environments/{environment_id}/"
            environment_data = FakeSquad.download_and_cache(
                general_data, self.session, environment_url
            )
            environment_json = json.loads(environment_data)
            self.environments[environment_id] = environment_json["slug"]
            return environment_json["slug"]
        else:
            return self.environments[environment_id]

        if environment_id == 16640:
            return "qemu_x86_64"
        if environment_id == 16643:
            return "x86"
        if environment_id == 16644:
            return "qemu_i386"
        if environment_id == 16646:
            return "i386"
        if environment_id == 16647:
            return "x86-kasan"
        if environment_id == 16650:
            return "x86_64-clang"
        if environment_id == 16654:
            return "qemu_arm64"
        if environment_id == 16657:
            return "dragonboard-410c"
        if environment_id == 16658:
            return "bcm2711-rpi-4-b"
        if environment_id == 16659:
            return "juno-r2"
        if environment_id == 16668:
            return "qemu-arm64-gic-version3"
        if environment_id == 16669:
            return "qemu-arm64-mte"
        if environment_id == 16671:
            return "arm"
        if environment_id == 16672:
            return "arm64"
        if environment_id == 16673:
            return "mips"
        if environment_id == 16674:
            return "powerpc"
        if environment_id == 16676:
            return "riscv"
        if environment_id == 16677:
            return "s390"
        if environment_id == 16678:
            return "sh"
        if environment_id == 16681:
            return "qemu-armv7"
        if environment_id == 16683:
            return "fvp-aemva"
        if environment_id == 16684:
            return "qemu-arm64"
        if environment_id == 16685:
            return "qemu-i386"
        if environment_id == 16687:
            return "qemu-riscv32"
        if environment_id == 16688:
            return "qemu-x86_64"
        return "Unknown"
